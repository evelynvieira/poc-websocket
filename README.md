
POC para comunicação Websocket entre servidor -> client

```bash
$ docker compose up -d
```

Para acessar o client
```
http://localhost:3000
```

Para enviar uma requisição para o servidor, precisamos passar qualquer objeto json no body da requisição (o endpoint aceita qualquer dado)

```
POST - http://localhost:3002/send
```