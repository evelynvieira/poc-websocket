import { Injectable } from '@nestjs/common';
import { AppGateway } from './app.gateway';

@Injectable()
export class AppService {
  private gateway: AppGateway;

  constructor(gateway: AppGateway) {
    this.gateway = gateway;
  }
  getHello(): string {
    return 'Hello World!';
  }

  handleMessage(data: Record<string, unknown>) {
    this.gateway.handleEvent(data);
  }
}
