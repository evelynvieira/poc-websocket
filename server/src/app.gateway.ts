import { Injectable } from '@nestjs/common';
import {
  MessageBody,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';

import { Server } from 'socket.io';

@Injectable()
@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class AppGateway implements OnGatewayInit {
  @WebSocketServer() server: Server = new Server();

  handleEvent(@MessageBody() data: Record<string, unknown>) {
    this.server.emit('events', data);
  }
  afterInit(server: any) {
    console.log('websocket initialized!!!');
  }
}
