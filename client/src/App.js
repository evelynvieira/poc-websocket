import { useEffect, useState } from 'react'
import { io } from 'socket.io-client';
import './App.css';

const socket = io('http://localhost:3002')

function App() {
  const [message, setMessage] = useState({});
  const [isConnected, setIsConnected] = useState(false);

  useEffect(() => {
    socket.on('connect', () => setIsConnected(true))
    socket.on('disconnect', () => setIsConnected(false))
    socket.on('events', (msg) => setMessage({ ...message, ...msg }))

    return () => {
      socket.off('connect')
      socket.off('disconnect')
      socket.off('events')
    }
  }, [])

  return (
    <div className="App">
      <h3>{isConnected ? 'Connected!' : 'Establishing connection...'}</h3>
      <div>
        <h5>Receiver</h5>
        <div 
          style={{ border: 'solid 3px black', backgroundColor: '#d3d3d3', padding: '2rem', display: 'inline-block', textAlign: 'initial'  }}>
            <pre>{JSON.stringify(message, null, 4)}</pre>
        </div>
      </div>
    </div>
  );
}

export default App;
